(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/',
      'home': '/home',
      'movements':'/movements/:id'
    
  }});

}(document));
